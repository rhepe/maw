# Management & Automation Architecture Workshop
Draft: 19.11.2020, V2.2 virtual, astolzen@redhat.com
## Abstract:
A technical Workshop for Partner Infrastructure/Cloud Architects covering all Infrastructure and Cloud Related Management Products:
 - Ansible Automation Platform with Core, Tower, Networking, Windows (and optional SAP) Automation
 - Insights, Satellite 
 - Advanced Ansible 

### This Workshop will cover the following topics:
 - Red Hat Insights
 - Automation with Ansible
 - Comparison between Ansible and competing products
 - Network & Security Automation with Ansible
 - Windows Automation with Ansible 
 - Red Hat Smart Management and Satellite
 - Advanced Ansible Topics
 - extensive Demo

### Prerequisites:
 - Linux Sysadmin Knowledge (LPC-1 Level)
 - basic knowledge of Bash or other scripting languages
 - RHCSA or RHCE appreciated, but not required

## AGENDA MAW 2.2 virtual
### Block 1 (3 Hours):

 - Introduction to Red Hat and the value of a subscription
 - Red Hat Insights
 - Introduction to Ansible Automation Plattform
 - Ansible Basics
 - Ansible Tower
 - Automation Hub, Collections and Galaxy

### Block 2 (3 Hours):

 - Ansible for Windows
 - Ansible for Networks and Security
 - (optional: Ansible for SAP)
 - Fully Automated Application Rollout, Demo and Code review

### Block 3 (3 Hours):

 - Red Hat Smart Management and Satellite
 - Ansible Integration with Satellite
 - Best Practices: Code in style 
 - Develop Automation as Team (aka: How to use Git for Ansible)
 - Advanced Execution (Debugging, Scale Out, distributed Networks etc..)
 - (future content: Automation Hub on premise)
 - Introduction to Red Hat Learning
 - Q&A

Optional Follow up: Q&A Session or Slack Channel Guidance for those who do self-paced Hands-On-Labs after Block 3.
